Feature: user should be able to login- with valid credentials

  Scenario: verify LoginPage
    Given open the browser
    Then the homepage of the website is displayed
    When user should enter valid "<USERNAME>" in email textbox
    And user should enter valid "<PASSWORD>" in password textbox
    Then the user should taken to homepage of demowebshop
    And close the browser

    
    Examples: 
      | USERNAME                     | PASSWORD | 
      | shankarsampath1056@gmail.com |     P@ssw0rd |
       