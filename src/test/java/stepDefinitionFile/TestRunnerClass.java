package stepDefinitionFile;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", glue = {
		"stepDefinitionFile" }, monochrome = true, plugin = { "pretty","html:target/HtmlReports" }

)

public class TestRunnerClass {

}
