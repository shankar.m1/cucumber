package stepDefinitionFile;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Demologin {

	static WebDriver driver = null;

	@Given("open the browser")
	public void open_the_browser() {
		System.out.println("launch the browser");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}

	@Then("the homepage of the website is displayed")
	public void the_homepage_of_the_website_is_displayed() {
		System.out.println("lanuch the homepage");
		driver.findElement(By.partialLinkText("Log in")).click();

	}

	@When("user should enter valid username in email textbox")
	public void user_should_enter_valid_username_in_email_textbox(String string) {
		System.out.println("enter the eamilid");
		driver.findElement(By.id("Email")).sendKeys(string);

	}

	@When("user should enter valid password in password textbox")
	public void user_should_enter_valid_password_in_password_textbox(String string) {
		System.out.println("enter the password");
		driver.findElement(By.id("Password")).sendKeys(string);
	}

	@Then("the user should taken to homepage of demowebshop")
	public void the_user_should_taken_to_homepage_of_demowebshop() {
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		System.out.println("launched the homepage Successfully ");

	}

	@Then("close the browser")
	public void close_the_browser() {
		System.out.println("close the browser");
		driver.close();
	}

}
